<!-- ======= Footer ======= -->
    <section class="section-footer">
        <div class="container">
            <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="widget-a">
                <div class="w-header-a">
                    <h3 class="w-title-a text-brand">Groupe <span class="color-b">SICAB</span></h3>
                </div>
                <div class="w-footer-a">
                    <ul class="list-unstyled">
                    <li class="color-a">
                        <span class="color-text-a">Phone .</span> contact@example.com
                    </li>
                    <li class="color-a">
                        <span class="color-text-a">Email .</span> +54 356 945234
                    </li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 section-md-t3">
                <div class="widget-a">
                <div class="w-header-a">
                    <h3 class="w-title-a text-brand">Notre compagnie</h3>
                </div>
                <div class="w-body-a">
                    <div class="w-body-a">
                    <ul class="list-unstyled">
                        <li class="item-list-a">
                        <i class="bi bi-chevron-right"></i> <a href="contact.php">Localisation</a>
                        </li>
                        <li class="item-list-a">
                        <i class="bi bi-chevron-right"></i> <a href="#">Politique privée</a>
                        </li>
                        <li class="item-list-a">
                        <i class="bi bi-chevron-right"></i> <a href="#">Affiliations</a>
                        </li>
                    </ul>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 section-md-t3">
                <div class="widget-a">
                <div class="w-header-a">
                    <h3 class="w-title-a text-brand">Nos branches étrangères</h3>
                </div>
                <div class="w-body-a">
                    <ul class="list-unstyled">
                    <li class="item-list-a">
                        <i class="bi bi-chevron-right"></i> <a href="#">France</a>
                    </li>
                    <li class="item-list-a">
                        <i class="bi bi-chevron-right"></i> <a href="#">China</a>
                    </li>
                    <li class="item-list-a">
                        <i class="bi bi-chevron-right"></i> <a href="#">Hong Kong</a>
                    </li>
                    </ul>
                </div>
                </div>
            </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <div class="socials-a">
                <ul class="list-inline">
                    <li class="list-inline-item">
                    <a href="#">
                        <i class="bi bi-facebook" aria-hidden="true"></i>
                    </a>
                    </li>
                    <li class="list-inline-item">
                    <a href="#">
                        <i class="bi bi-twitter" aria-hidden="true"></i>
                    </a>
                    </li>
                    <li class="list-inline-item">
                    <a href="#">
                        <i class="bi bi-instagram" aria-hidden="true"></i>
                    </a>
                    </li>
                    <li class="list-inline-item">
                    <a href="#">
                        <i class="bi bi-linkedin" aria-hidden="true"></i>
                    </a>
                    </li>
                </ul>
                </div>
                <div class="copyright-footer">
                    <p class="copyright color-text-a">
                        &copy; Amélioré par
                        <span class="color-a">Groupe SICAB</span> Tout droits reservés.
                    </p>

                    <p class="copyright color-text-a">
                        &copy; Designed by
                        <span class="color-a"><a href="https://bootstrapmade.com/">BoostrapMade</a></span> All rights reserved
                    </p>
                </div>
            </div>
            </div>
        </div>
    </footer>
<!-- End  Footer -->