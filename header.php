<!-- ======= All Headers Section======= -->
    
    <!-- ======= Property Search Section ======= -->
    <div class="click-closed"></div>
    <!--/ Form Search Star /-->
    <div class="box-collapse">
      <div class="title-box-d">
        <h3 class="title-d">Connexion</h3>
      </div>
      <span class="close-box-collapse right-boxed bi bi-x"></span>
      <div class="box-collapse-wrap form">
        <form class="form-a">
          <div class="row">
            <!--Input Part-->
            <div class="col-md-12 mb-2">
              <div class="form-group">
                <label class="pb-2" for="Type">Username ou email</label>
                <input type="text" class="form-control form-control-lg form-control-a" placeholder="">
              </div>
            </div>

            <div class="col-md-12 mb-2">
              <div class="form-group">
                <label class="pb-2" for="Type">Mot de Passe</label>
                <input type="password" class="form-control form-control-lg form-control-a" placeholder="">
              </div>
            </div>
            <!--Close Input Part-->

            <!--Validation and Register Part-->
            <div class="col-md-12">
              <button type="submit" class="btn btn-b">Se Connecter</button>
              <p class="mt-4">
                Vous n'avez pas de compte ?
                <button type="submit" class="btn btn-outline-secondary">Cliquez pour vous inscrire</button>
              </p>
            </div>
            <!--Close Validation and Register Part-->
          </div>
        </form>
      </div>
    </div><!-- End Property Search Section -->>

    <!-- ======= Header/Navbar ======= -->
    <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
      <div class="container">
        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarDefault"
          aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span></span>
          <span></span>
          <span></span>
        </button>
        <a class="navbar-brand text-brand" href="index.php">Groupe <span class="color-b">SICAB</span></a>

        <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link active" href="index.php">Accueil</a>
            </li>

            <li class="nav-item">
              <a class="nav-link " href="a_propos.php">A propos</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Nos secteurs</a>
              <div class="dropdown-menu">
                <a class="dropdown-item " href="Commerce.php">Commerce</a>
                <a class="dropdown-item " href="Agriculture.php">Agriculture</a>
                <a class="dropdown-item " href="Batiment.php">Batiment</a>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link " href="contact.php">Contact</a>
            </li>
          </ul>
        </div>

        <button type="button" class="btn btn-b-n navbar-toggle-box navbar-toggle-box-collapse" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01">
          <img src="assets/img/login.png" alt="" style="width: 25px;">
        </button>
      </div>
    </nav><!-- End Header/Navbar -->
<!-- ======= End All Headers Section======= -->
